#!/bin/bash
# Check if the ECS service exists
FAILURE=$(aws ecs describe-services --cluster ${DEV_CLUSTER} --services ${DEV_SERVICE} --query "failures[0]" --output text)

if [ "$FAILURE" != "None" ]; then
  echo "ECS service ${DEV_SERVICE} does not exist"
  exit 1
fi

# Check if the ECS service is inactive
ECS_SERVICE_STATUS=$(aws ecs describe-services --cluster ${DEV_CLUSTER} --services ${DEV_SERVICE} --query 'services[0].status' --output text)
if [ "$ECS_SERVICE_STATUS" != "ACTIVE" ]; then
  echo "ECS service ${DEV_SERVICE} is inactive"
  exit 1
fi

echo "ECS service ${DEV_SERVICE} is active"
exit 0

# if ! $(./scripts/check_ecs.sh >/dev/null);then echo a;fi

# aws ecs create-service \
#     --cluster ${DEV_CLUSTER} \
#     --service-name ${DEV_SERVICE} \
#     --desired-count 1 \
#     --task-definition dev-web-app-definition \
#     --launch-type FARGATE \
#     --platform-version LATEST \
#     --network-configuration "awsvpcConfiguration={subnets=[subnet-0344d44130e11b79c,subnet-0de56d1e9c2d97060,subnet-02bbfe46ba29ce20a],securityGroups=[sg-04741693a6494256c],assignPublicIp=ENABLED}" \
#    